/*-------------------------------------------------------------------------
  Program to find the most direct route in an airline network from a given
  start city to a given destination city.  A digraph represented by its
  adjacency-list implementation is used for the network, and the
  information needed to construct it is read from networkFile.
 -------------------------------------------------------------------------*/
 
#include "Digraph.h"
#include <cstdlib>
using namespace std;


int main(int argc, char *argv[]) {
  if (argc < 2) { 
    cout << "Usage: " << argv[0] << " <Graph File>" << endl;
    exit(1);
  }

 
  ifstream inFile(argv[1]);
  if (!inFile.is_open())
  {
    cerr << "*** Cannot open " << argv[1] << " ***\n";
    exit(-1);
  }

  Digraph d;
  d.graphRead(inFile);
  cout << "The Digraph's ";
  d.display(cout);
  cout << endl;

  d.outputGraphviz();

  cout << endl << "There are " << d.connected() << " connected vertices on the graph." << endl;
  
  int min, max;
  d.minmaxDegree(min, max);
  cout << "Maximun degree: " << max << endl << "Minimum degree: " << min <<endl;

  int start, destination;
  char response;
  do
  {
    cout << "Number of start city? ";
    cin >> start;
    cout << "Number of destination? ";
    cin >> destination;

    vector<int> path = d.shortestPath(start, destination);
    cout << "Shortest path is:\n";
    for (int k = 0; k < path.size()-1; k++)
    {
      cout <<  path[k] << " " << d.getData(path[k]) << endl;
      cout << "      |\n"
              "      v\n";
    }
    cout << destination << ' ' << d.getData(destination) << endl;
    cout << "\nMore (Y or N)?";
    cin >> response;
  }
  while (response == 'y' || response == 'Y');
}
