/*--- Digraph.cpp ---------------------------------------------------------
 Implementation of the Digraph class  
 -------------------------------------------------------------------------*/

#include "Digraph.h"
#include <iostream>
#include <string>
using namespace std;


//--- Definition of data()
string Digraph::getData(int k) const { 
  return Vertices[k].data;
}



void Digraph::InsertNode(string st) { 
   Vertices.push_back(Vertex(st));
}

void Digraph::InsertEdge(int from, int to) { 
  if (from < 0 || to < 0) {
    cout << "Warning in InsertEdge: invalid node index " 
        << from << ", " << to << endl;
    return;
  }
  Vertices[from].adjacencyList.push_back(to);
}

//--- Definition of read()
void Digraph::read(ifstream & inStream) {
  //Digraph::VertexInfo vi;
  int n,           // number of vertices adjacent to some vertex
      vertex;      // the number of a vertex
  string st;
   
  int ctr = 0;
  for (;;) {
    
    inStream >> st;
    if (inStream.eof()) break;
    InsertNode(st);

    inStream >> n;
    
    for (int i = 1; i <= n; i++) {
      inStream >> vertex;
      InsertEdge(ctr, vertex);
    }
    ctr++;
  }
  
}

//--- Definition of display()
void Digraph::display(ostream & out)
{
  out << "Adjacency-List Representation: \n";
  for (int i = 0; i < Vertices.size(); i++) {
    out << i << ": " <<  Vertices[i].data << "--";
    
    for (vector<int>::iterator 
          it = Vertices[i].adjacencyList.begin();
          it != Vertices[i].adjacencyList.end(); it++)
      out << *it << "  ";
    
    out << endl;
  }
}
  

void Digraph::depthFirstSearch(int start, vector<bool> & unvisited)
{
  // Add statements here to process Vertices[start].data
  cout << Vertices[start].data << endl;

  unvisited[start] = false;
  // Traverse its adjacency list, performing depth-first 
  // searches from each unvisited vertex in it.
  for (vector<int>::iterator 
         it = Vertices[start].adjacencyList.begin();
         it != Vertices[start].adjacencyList.end(); it++)
    // check if current vertex has been visited
    if (unvisited[*it])
      // start DFS from new node
      depthFirstSearch(*it, unvisited); 
}

//-- Definitions of depthFirstSearch() and depthFirstSearchAux()
inline void Digraph::depthFirstSearch(int start) {
  vector<bool> unvisited(Vertices.size(), true);
  depthFirstSearch(start, unvisited);
}


//--- Definition of shortestPath()
vector<int> Digraph::shortestPath(int start, int destination)
{
  int n = Vertices.size(); // number of vertices (#ed from 1)
  vector<int> distLabel(n,-1),     // distance labels for vertices, all
                                   // marked as unvisited (-1)
              predLabel(n);        // predecessor labels for vertices
  // Perform breadth first search from start to find destination,
  // labeling vertices with distances from start as we go.
  distLabel[start] = 0;
  int distance = 0,                // distance from start vertex
      vertex;                      // a vertex
  queue<int> vertexQueue;          // queue of vertices

  vertexQueue.push(start);
  
  while (distLabel[destination] < 0 && !vertexQueue.empty()){
    vertex = vertexQueue.front();
    
    vertexQueue.pop();
    if (distLabel[vertex] > distance)
      distance++;
    for (vector<int>::iterator 
         it = Vertices[vertex].adjacencyList.begin();
         it != Vertices[vertex].adjacencyList.end(); it++)
      
      if (distLabel[*it] < 0)       {
        distLabel[*it] = distance + 1;
        predLabel[*it] = vertex;
        vertexQueue.push(*it);
      }
  }
  distance++;

  // Now reconstruct the shortest path if there is one
  vector<int> path(distance+1);
  if (distLabel[destination] < 0)
    cout << "Destination not reachable from start vertex\n";
  else   {
    path[distance] = destination;
    for (int k = distance - 1; k >= 0; k--)
      path[k] = predLabel[path[k+1]];
  }

  return path;
} 

void Digraph::outputGraphviz() const {
  cout << "digraph D { \n";
  for (int i = 0; i < Vertices.size(); i++) {
    if ((Vertices[i].adjacencyList.size() == 0) && i != 0){
      cout << i << "->;" << endl;
    }
    for (int j = 0; j < Vertices[i].adjacencyList.size(); j++) 
      cout << i << "->" << Vertices[i].adjacencyList[j] << ";\n";
  }
  cout << "}" << endl;
} 

void Digraph::graphRead(ifstream &document){
  string nodeName;
  nodeName = "";
  //Reads which numbers from to start and to end
  int from, to, nodeNum, edgeNum; 
  document >> nodeNum; //Reads ans ignores the first
  document >> nodeNum >> edgeNum;
  InsertNode(nodeName);
  for (int i =0; i < nodeNum; i++){
    InsertNode(nodeName);
  }
  for(int j =0; j < edgeNum; j++){
    document >> from >> to;
    InsertEdge(from, to);
    InsertEdge(to, from);
  }
}

int Digraph::connected(){
  // We create an array with lenght of the quantity of vertex
  std::vector<bool> C(Vertices.size(), true);

  // Since we will took -1 to represent a no-visited vertex, we inicialize
  // the array to that value
  //for (int i = 0; i < Vertices.size(); i++) C[i] = -1;

  // We need a counter which count the number of groups of vertex connected
  int connections = 0;

  // We go throughout the array: if it has a -1 we do a traversal starting in
  // that index
  for (int i = 0; i < Vertices.size(); i++){
    if (C[i] == true){
      depthFirstSearch(i, C);
      connections++;
    }
  }
  //It returns one less because it takes into consideration the index 0, which we aren't using
  return connections-1;
}

void Digraph::minmaxDegree(int &min, int &max){
  int tmp;
  min = max = Vertices[1].adjacencyList.size();

  for (int i = 2; i < Vertices.size() ; i++){
    tmp = Vertices[i].adjacencyList.size();
    if (tmp < min) min = tmp;
    if (tmp > max) max = tmp;
  }
}
